Taskman::Application.routes.draw do
  root :to => 'welcome#index'

  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  get 'tasks', :to => 'tasks#index', :as => :user_root

  resources :tasks, :only => [:index, :create ] do
    put :complete,   :on => :member
    put :uncomplete, :on => :member
    get :done,       :on => :collection
  end
end
