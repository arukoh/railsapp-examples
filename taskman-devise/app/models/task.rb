class Task < ActiveRecord::Base
  belongs_to :user

  attr_accessible :user, :done, :due_date, :name
  scope :done,   where(:done => true).order(:due_date)
  scope :undone, where(:done => false).order(:due_date)
end
