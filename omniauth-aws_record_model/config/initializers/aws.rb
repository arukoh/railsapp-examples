AWS.config(
  :simple_db_endpoint => 'sdb.ap-northeast-1.amazonaws.com',
  :proxy_uri          => ENV['HTTPS_PROXY'] || ENV['https_proxy'] || ENV['HTTP_PROXY'] || ENV['http_proxy']
)
AWS::Record.domain_prefix = 'omniauth-'
