class User < AWS::Record::Model
  set_shard_name Rails.env

  string_attr :name
  string_attr :provider
  string_attr :uid

  timestamps

  class << self
    def create_with_omniauth(auth)
      user = User.new
      user.name     = auth["info"]["nickname"]
      user.provider = auth["provider"]
      user.uid      = auth["uid"]
      user.save
      user
    end

    def find_by_provider_and_uid provider, uid
      User.find(:first, :where => { :provider => provider, :uid => uid })
    end
  end

end
