AWS.config(
  :dynamo_db_endpoint => 'dynamodb.ap-northeast-1.amazonaws.com',
  :proxy_uri          => ENV['HTTPS_PROXY'] || ENV['https_proxy'] || ENV['HTTP_PROXY'] || ENV['http_proxy']
)
AWS::Record.table_prefix = 'omniauth-'
