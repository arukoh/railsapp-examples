Omniauth::Application.routes.draw do
  #OAuth$B%3!<%k%P%C%/(B
  match "/auth/:provider/callback" => "sessions#callback"
  #OAuth$B%m%0%"%&%H(B
  match "/logout" => "sessions#destroy", :as => :logout

  root :to => 'welcome#index'
end
