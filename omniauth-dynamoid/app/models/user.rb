class User
  include Dynamoid::Document
  field :name
  field :provider
  field :uid

  class << self
    def create_with_omniauth(auth)
      create!(
        :name     => auth["info"]["nickname"],
        :provider => auth["provider"],
        :uid      => auth["uid"]
      )
    end
  end

end
