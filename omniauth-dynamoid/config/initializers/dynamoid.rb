require 'aws'
AWS.config :proxy_uri => ENV['HTTPS_PROXY'] || ENV['https_proxy'] || ENV['HTTP_PROXY'] || ENV['http_proxy']

Dynamoid.configure do |config|
  # config.adapter = 'local' # This adapter allows offline development without connecting to the DynamoDB servers. Data is *NOT* persisted.
  config.adapter = 'aws_sdk' # This adapter establishes a connection to the DynamoDB servers using Amazon's own AWS gem.
  # config.access_key = 'access_key' # If connecting to DynamoDB, your access key is required.
  # config.secret_key = 'secret_key' # So is your secret key.
  config.endpoint = 'dynamodb.ap-northeast-1.amazonaws.com' # Set the regional endpoint for DynamoDB.
  config.namespace = "dynamoid_app_development" # To namespace tables created by Dynamoid from other tables you might have.
  config.warn_on_scan = true # Output a warning to the logger when you perform a scan rather than a query on a table.
  config.partitioning = true # Spread writes randomly across the database. See "partitioning" below for more.
  config.partition_size = 200  # Determine the key space size that writes are randomly spread across.
  config.read_capacity = 3 # Read capacity for your tables
  config.write_capacity = 5 # Write capacity for your tables
end
