Taskman::Application.routes.draw do
  resources :tasks, :only => [:index, :create ] do
    put :complete,   :on => :member
    put :uncomplete, :on => :member
    get :done,       :on => :collection
  end
end
