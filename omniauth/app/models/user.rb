class User < ActiveRecord::Base
  attr_accessible :name, :provider, :uid

  class << self
    def create_with_omniauth(auth)
      create! do |user|
        user.name     = auth["info"]["nickname"]
        user.provider = auth["provider"]
        user.uid      = auth["uid"]
      end
    end
  end

end
