# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Omniauth::Application.initialize!

Rails.application.config.active_record.timestamped_migrations = false
